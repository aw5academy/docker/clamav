#!/bin/bash

SQS_RESPONSE_FILE=/opt/clamav/sqs.json
SQS_MESSAGES_DIR=/opt/clamav/sqs
S3_DOWNLOAD_DIR=/opt/clamav/s3

validateEnvVars() {
  if [ ! "$QUEUE_URL" ]; then
    printf "No QUEUE_URL env var found!\n"
    exit 1
  fi
  if [ ! "$AWS_REGION" ]; then
    printf "No AWS_REGION env var found!\n"
    exit 1
  fi
  if [ ! "$SQS_MAX_RECEIVE_MESSAGES" ]; then
    printf "No SQS_MAX_RECEIVE_MESSAGES env var found!\n"
    exit 1
  fi
}

startClamd() {
  sudo /usr/sbin/clamd -c /etc/clamd.d/scan.conf
}

processSqsMessages() {
  while true; do
    rm -f $SQS_RESPONSE_FILE $SQS_MESSAGES_DIR/* $S3_DOWNLOAD_DIR/*
    recieveSqsMessages
    if [ ! -s $SQS_RESPONSE_FILE ]; then
      continue
    fi
    saveMessagesToProcess
    local sqs_message_file
    for sqs_message_file in $SQS_MESSAGES_DIR/*; do
      bash /home/clamav/clamdscan-worker.sh "$sqs_message_file"&
    done
    wait
  done
}

recieveSqsMessages() {
  aws sqs receive-message --queue-url $QUEUE_URL --region $AWS_REGION --endpoint-url https://sqs.${AWS_REGION}.amazonaws.com/ --max-number-of-messages $SQS_MAX_RECEIVE_MESSAGES | tee $SQS_RESPONSE_FILE
}

saveMessagesToProcess() {
  local message
  local message_index=0
  for message in $(cat $SQS_RESPONSE_FILE |jq -c '.Messages' | jq -r '.[] | @base64'); do
    message_index=$(($message_index + 1))
    echo "$message" | base64 --decode > $SQS_MESSAGES_DIR/$message_index
  done
}

validateEnvVars
startClamd
processSqsMessages
