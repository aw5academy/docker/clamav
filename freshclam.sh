#!/bin/bash

if [ ! -d /mnt/efs ]; then
  printf "No directory at /mnt/efs! Possible EFS mount failure!\n"
  exit 1
fi

sudo /bin/freshclam
if [ "$?" == "0" ]; then
  printf "SUCESS! Virus database updated.\n"
fi
