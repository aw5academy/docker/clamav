#!/bin/bash

SQS_MESSAGE_FILE="$1"
S3_DOWNLOAD_DIR=/opt/clamav/s3

validateEnvVars() {
  if [ ! "$QUEUE_URL" ]; then
    printf "No QUEUE_URL env var found!\n"
    exit 1
  fi
  if [ ! "$AWS_REGION" ]; then
    printf "No AWS_REGION env var found!\n"
    exit 1
  fi
}

validateInputs() {
  if [ ! "$SQS_MESSAGE_FILE" ]; then
    printf "No SQS_MESSAGE_FILE arg found!\n"
    exit 1
  fi
  if [ ! -e $SQS_MESSAGE_FILE ]; then
    printf "No file found at ${SQS_MESSAGE_FILE}!\n"
    exit 1
  fi
}

processSqsMessage() {
  getSqsMessageDetails
  if [[ ! "$SQS_MESSAGE_ID" ]] || [[ ! "$SQS_RECEIPT_HANDLE" ]]; then
    printf "ERROR: Unable to retrieve SQS message ID or receipt handle!\n"
    return 0
  fi
  checkIfS3TestEvent
  if [ "$S3_TEST_EVENT" == "true" ]; then
    deleteSqsMessage
    return 0
  fi
  getS3BucketNameAndObjectKey
  if [[ ! "$S3_BUCKET_NAME" ]] || [[ ! "$S3_OBJECT_KEY" ]]; then
    printf "ERROR: Unable to retrieve S3 bucket name or object key!\n"
    return 0
  fi
  downloadObjectFromS3
  if [ "$S3_DOWNLOAD_RESPONSE" != "0" ]; then
    printf "ERROR: Unable to download s3://${S3_BUCKET_NAME}/${S3_OBJECT_KEY}!\n"
    return 0
  fi
  scanFile
  if [ "$AV_STATUS" == "ERROR" ]; then
    printf "ERROR: clamdscan encountered an error!\n"
    return 0
  fi
  updateS3ObjectTags
  if [ "$S3_TAG_RESPONSE" != "0" ]; then
    printf "ERROR: Unable to tag s3://${S3_BUCKET_NAME}/${S3_OBJECT_KEY}!\n"
    return 0
  fi
  deleteSqsMessage
}

getSqsMessageDetails() {
  SQS_MESSAGE_ID="$(cat $SQS_MESSAGE_FILE | jq -r .MessageId)"
  SQS_RECEIPT_HANDLE="$(cat $SQS_MESSAGE_FILE | jq -r .ReceiptHandle)"
}

checkIfS3TestEvent() {
  if [ "$(cat $SQS_MESSAGE_FILE | jq -r .Body |jq -r .Event)" == "s3:TestEvent" ]; then
    S3_TEST_EVENT="true"
  fi
}

getS3BucketNameAndObjectKey() {
  S3_BUCKET_NAME="$(cat $SQS_MESSAGE_FILE | jq -r .Body |jq -r .Records[].s3.bucket.name)"
  S3_OBJECT_KEY="$(cat $SQS_MESSAGE_FILE | jq -r .Body |jq -r .Records[].s3.object.key)"
}

downloadObjectFromS3() {
  aws s3 cp s3://${S3_BUCKET_NAME}/${S3_OBJECT_KEY} $S3_DOWNLOAD_DIR/$SQS_MESSAGE_ID
  S3_DOWNLOAD_RESPONSE="$?"
  chmod 644 $S3_DOWNLOAD_DIR/$SQS_MESSAGE_ID
}

scanFile() {
  clamdscan --remove $S3_DOWNLOAD_DIR/$SQS_MESSAGE_ID
  case "$?" in
    0)
      AV_STATUS="CLEAN"
    ;;
    1)
      AV_STATUS="INFECTED"
    ;;
    *)
      AV_STATUS="ERROR"
    ;;
  esac
}

updateS3ObjectTags() {
  printf "Tagging s3://$S3_BUCKET_NAME/$S3_OBJECT_KEY with av-status=${AV_STATUS}...\n"
  aws s3api put-object-tagging \
    --bucket $S3_BUCKET_NAME \
    --key "$S3_OBJECT_KEY" \
    --tagging "{\"TagSet\": [{ \"Key\": \"av-status\", \"Value\": \"$AV_STATUS\" } ]}"
  S3_TAG_RESPONSE="$?"
}

deleteSqsMessage() {
  printf "Deleting SQS message with message id <$SQS_MESSAGE_ID>...\n"
  aws sqs delete-message --queue-url $QUEUE_URL --region $AWS_REGION --endpoint-url https://sqs.${AWS_REGION}.amazonaws.com/ --receipt-handle $SQS_RECEIPT_HANDLE
}

validateEnvVars
validateInputs
processSqsMessage
