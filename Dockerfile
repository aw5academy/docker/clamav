FROM amazonlinux:latest

RUN amazon-linux-extras install -y epel && yum -y install clamav-server clamav-data clamav-update clamav-filesystem clamav clamav-scanner-systemd clamav-devel clamav-lib clamav-server-systemd && \
    yum -y install jq && \
    amazon-linux-extras install -y python3.8 && \
    python3.8 -m pip install awscli && \
    yum -y install shadow-utils sudo && \
    yum clean all && \
    mkdir -p /etc/sudoers.d && chmod 755 /etc/sudoers.d && \
    useradd -ms /bin/bash clamav && \
    chage -m 0 -M 99999 -I -1 -E -1 clamav && \
    echo "clamav ALL= NOPASSWD: /usr/sbin/clamd -c /etc/clamd.d/scan.conf" >> /etc/sudoers.d/clamav && \
    echo "clamav ALL= NOPASSWD: /bin/freshclam" >> /etc/sudoers.d/clamav && \
    usermod -a -G virusgroup clamav && \
    mkdir -p /opt/clamav/sqs /opt/clamav/s3 && chown -R clamav:virusgroup /opt/clamav

ADD start.sh /home/clamav/start.sh
ADD freshclam.sh /home/clamav/freshclam.sh
ADD clamdscan.sh /home/clamav/clamdscan.sh
ADD clamdscan-worker.sh /home/clamav/clamdscan-worker.sh
ADD freshclam.conf /etc/freshclam.conf
ADD scan.conf /etc/clamd.d/scan.conf

RUN chmod +x /home/clamav/start.sh && \
    chmod +x /home/clamav/freshclam.sh && \
    chmod +x /home/clamav/clamdscan.sh && \
    chmod +x /home/clamav/clamdscan-worker.sh && \
    chmod 600 /etc/freshclam.conf && \
    chmod 644 /etc/clamd.d/scan.conf

USER clamav
CMD ["/home/clamav/start.sh"]
